import json
import os
import re
from datetime import datetime
from decimal import Decimal
from glob import glob
from pathlib import Path
from shutil import rmtree
from subprocess import Popen, check_output

import requests
from decouple import config as envconfig


def walk_keys(obj, path=""):
    if isinstance(obj, dict):
        for k, v in obj.items():
            yield from walk_keys(v, path + "." + k if path else k)
    else:
        yield path, obj


def is_url(url):
    regex = re.compile(
        r"^(?:http|ftp)s?://"  # http:// or https://
        r"(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|"  # domain...
        r"localhost|"  # localhost...
        r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})"  # ...or ip
        r"(?::\d+)?"  # optional port
        r"(?:/?|[/?]\S+)$",
        re.IGNORECASE,
    )
    return re.match(regex, url) is not None


def is_downloadable(url):
    h = requests.head(url, allow_redirects=True)
    header = h.headers
    content_type = header.get("content-type")
    if "text" in content_type.lower():
        return False
    if "html" in content_type.lower():
        return False
    return True


class Job:
    STATUS_SUBMITTED = "S"
    STATUS_QUEUED = "Q"
    STATUS_TERMINATED = "T"
    STATUS_CANCELLED = "C"
    STATUS_DONE = "D"
    STATUS_FAILED = "F"
    STATUS_RUNNING = "R"
    MODEL_CORONAL = "C"
    MODEL_HELIOSPHERIC = "H"

    def __init__(
        self,
        job,
        runs_path=envconfig(
            "EUHFORIAONLINE_RUNS_PATH", default=os.path.join(envconfig("VSC_SCRATCH"), "euhforiaonline", "runs")
        ),
        wice_euhforia_path=envconfig(
            "EUHFORIAONLINE_WICE_EUHFORIA_PATH",
            default="/data/leuven/projects/vswmc/test/VSWMC_MODEL/euhforia",
        ),
        graphql_endpoint=envconfig(
            "EUHFORIAONLINE_GRAPHQL_ENDPOINT", default="https://api.euhforiaonline.com/graphql/"
        ),
        default_account=envconfig("EUHFORIAONLINE_DEFAULT_ACCOUNT"),
    ):
        self._job = job
        self._run_name = self._job["run"]["config"]["name"].strip().replace(" ", "-")
        self._path = os.path.join(runs_path, "{}-{}".format(self._job["run"]["id"], self._run_name))
        self._corona_path = os.path.join(self._path, "corona")
        self._heliosphere_path = os.path.join(self._path, "heliosphere")
        self._solar_wind_boundary_path = os.path.join(
            self._corona_path, "solar-wind-boundary-{}-{}.dat".format(self._job["run"]["id"], self._run_name)
        )
        self._corona_config_path = os.path.join(
            self._corona_path, "corona-{}-{}.cfg".format(self._job["run"]["id"], self._run_name)
        )
        self._heliosphere_config_path = os.path.join(
            self._heliosphere_path, "heliosphere-{}-{}.cfg".format(self._job["run"]["id"], self._run_name)
        )
        self._cmes_config_path = os.path.join(
            self._heliosphere_path, "cmes-{}-{}.dat".format(self._job["run"]["id"], self._run_name)
        )
        self._cluster_config_path = os.path.join(
            self._heliosphere_path, "cluster-{}-{}-heliosphere.slurm".format(self._job["run"]["id"], self._run_name)
        )
        self._cluster_corona_config_path = os.path.join(
            self._corona_path, "cluster-{}-{}-corona.slurm".format(self._job["run"]["id"], self._run_name)
        )
        # if self._job["run"]["config"]["heliospheric_model"]["cluster"]["ppn"] == 72:
        self._euhforia_path = wice_euhforia_path
        self._graphql_endpoint = graphql_endpoint
        self._default_account = default_account

    def _get_stack(self):
        stack = [
            "module --force purge",
            "module load cluster/wice/batch",
            "module use /data/leuven/projects/vswmc/test/VSWMC_MODEL/euhforia/easybuild/modules/all/",
            "module load EUHFORIA",
        ]
        return stack

    def is_coronal(self):
        return self._job["model"] == self.MODEL_CORONAL

    def is_heliospheric(self):
        return self._job["model"] == self.MODEL_HELIOSPHERIC

    def to_run_coronal(self):
        return self._job["run"]["config"]["coronal_model"]["to_run"]

    def to_run_heliospheric(self):
        return self._job["run"]["config"]["heliospheric_model"]["to_run"]

    def create_heliospheric_job(self, secret_key=envconfig("EUHFORIAONLINE_SECRET_KEY")):
        query = """
            mutation($jobData: JobInput!) {
                createJob(jobData: $jobData) {
                    id
                    model
                    status
                    jobId
                    run {
                        id
                        config
                    }
                    user {
                        id
                        email
                    }
                    updatedAt
                }
            }
        """
        variables = {
            "jobData": {
                "userId": self._job["user"]["id"],
                "runId": self._job["run"]["id"],
                "model": self.MODEL_HELIOSPHERIC,
                "status": self.STATUS_SUBMITTED,
            }
        }
        json_response = requests.post(
            self._graphql_endpoint, json={"query": query, "variables": variables}, headers={"Authorization": secret_key}
        ).json()
        job_config = dict(json_response["data"]["createJob"])
        job_config["run"]["config"] = json.loads(job_config["run"]["config"])
        return Job(job_config)

    def update(
        self,
        status=None,
        job_id=None,
        secret_key=envconfig("EUHFORIAONLINE_SECRET_KEY"),
    ):
        query = """
            mutation($id: ID!, $jobData: JobInput!) {
                updateJob(id: $id, jobData: $jobData) {
                    id
                }
            }
        """
        variables = {"id": self._job["id"], "jobData": {}}
        if status is not None:
            variables["jobData"]["status"] = status
        if job_id is not None:
            variables["jobData"]["jobId"] = job_id
        json_response = requests.post(
            self._graphql_endpoint, json={"query": query, "variables": variables}, headers={"Authorization": secret_key}
        ).json()
        return json_response

    def configure_coronal_model(self):
        Path(self._corona_path).mkdir(parents=True, exist_ok=True)
        with open(self._corona_config_path, "w") as config_file:
            config = dict(self._job["run"]["config"]["coronal_model"])
            config["Magnetogram"]["data_dir"] = "{}/".format(self._corona_path)
            config["HeliosphereBoundary"]["output_file_name"] = self._solar_wind_boundary_path
            config.pop("to_run", None)
            config["Magnetogram"].pop("date", None)
            if config["Magnetogram"]["provider"] == "custom":
                config["Magnetogram"]["provider"] = "GONG"
            for section_name, section_content in config.items():
                config_file.write("[{}]\n".format(section_name))
                for param_name, param_value in section_content.items():
                    for key, value in walk_keys(param_value, param_name):
                        config_file.write("{} = {}\n".format(key, value))
        with open(self._cluster_corona_config_path, "w") as config_file:
            config_file.write("#!/bin/bash -l\n")
            config_file.write("#SBATCH --account={}\n".format(self._default_account))
            config_file.write("#SBATCH --cluster=wice\n")
            config_file.write("#SBATCH --nodes=1\n")
            config_file.write("#SBATCH --time=00:30:00\n")
            config_file.write("#SBATCH --output={}\n".format(os.path.join(self._path, "log.txt")))
            config_file.write("#SBATCH --job-name=euhforiaonline-{}\n".format(self._job["id"]))

            for line in self._get_stack():
                config_file.write("{}\n".format(line))

            config_file.write(
                "python -W ignore {} --config-file {} --create-plots >> {} 2>&1\n".format(
                    os.path.join(self._euhforia_path, "run", "empirical_coronal_model.py"),
                    self._corona_config_path,
                    os.path.join(self._path, "log.txt"),
                )
            )

    def get_cme_insertion(self):
        if len(self._job["run"]["config"]["heliospheric_model"]["cmes"]) == 0:
            return 0
        else:
            earliest_cme_dt = min(
                [
                    datetime.strptime(cme["datetime"], "%Y-%m-%dT%H:%M:%S")
                    for cme in self._job["run"]["config"]["heliospheric_model"]["cmes"]
                ]
            )
            with open(self._solar_wind_boundary_path) as fp:
                # reading datetime from the second line of the file
                next(fp)
                solar_wind_boundary_dt = datetime.strptime(next(fp).rstrip(), "%Y-%m-%dT%H:%M:%S")
            return (solar_wind_boundary_dt - earliest_cme_dt).days + 1

    def configure_heliospheric_model(self):
        Path(self._heliosphere_path).mkdir(parents=True, exist_ok=True)
        with open(self._heliosphere_config_path, "w") as config_file:
            config = dict(self._job["run"]["config"]["heliospheric_model"])
            if is_url(config["Data"]["solar_wind"]) and is_downloadable(config["Data"]["solar_wind"]):
                Path(self._corona_path).mkdir(parents=True, exist_ok=True)
                response = requests.get(config["Data"]["solar_wind"], allow_redirects=True)
                open(self._solar_wind_boundary_path, "wb").write(response.content)
            config["Data"]["solar_wind"] = self._solar_wind_boundary_path
            config["Data"]["cmes"] = self._cmes_config_path
            config["Duration"]["cme_insertion"] = self.get_cme_insertion()
            config["Output"] = {}
            config["Output"]["base_name"] = "{}_{}".format(self._job["run"]["id"], self._run_name)
            config["Output"]["directory"] = "{}/".format(self._heliosphere_path)
            config["Output.NPY"] = {}
            config["Output.NPY"]["interval"] = 1
            config["Output.NPY"]["start"] = "insertion"
            sc_index = 1
            for _, virtual_spacecraft in enumerate(config["VirtualSpacecraft"]):
                if (
                    virtual_spacecraft["shift_r"] == 0
                    and virtual_spacecraft["shift_lon"] == 0
                    and virtual_spacecraft["shift_lat"] == 0
                ):
                    config["VirtualSpacecraft.{}".format(virtual_spacecraft["relative_to"])] = {
                        "start": virtual_spacecraft["start"]
                    }
                else:
                    config["VirtualSpacecraft.SC{}".format(sc_index)] = {
                        "relative_to": virtual_spacecraft["relative_to"],
                        "start": virtual_spacecraft["start"],
                        "shift": "{{{}, {}, {}}}".format(
                            virtual_spacecraft["shift_r"],
                            virtual_spacecraft["shift_lon"],
                            virtual_spacecraft["shift_lat"],
                        ),
                    }
                    sc_index = sc_index + 1
            config["VirtualSpacecraft"] = {}
            config["VirtualSpacecraft"]["interval"] = 10
            config.pop("to_run", None)
            config.pop("cmes", None)
            config.pop("plotting", None)
            config.pop("cluster", None)
            for section_name, section_content in config.items():
                config_file.write("[{}]\n".format(section_name))
                if isinstance(section_content, dict):
                    for param_name, param_value in section_content.items():
                        for key, value in walk_keys(param_value, param_name):
                            config_file.write("{} = {}\n".format(key, value))
        # cmes config
        with open(self._cmes_config_path, "w") as config_file:
            for cme in self._job["run"]["config"]["heliospheric_model"]["cmes"]:
                if cme["model"] == "Cone":
                    config_file.write(
                        "{} {} {:.2f} {:.2f} {:.2f} {:.2f} {:.2e} {:.2e}\n".format(
                            cme["model"],
                            cme["datetime"],
                            float(cme["latitude"]),
                            float(cme["longitude"]),
                            float(cme["halfWidth"]),
                            float(cme["speed"]),
                            Decimal(cme["density"]),
                            Decimal(cme["temperature"]),
                        )
                    )
                elif cme["model"] == "LFFSpheromak":
                    config_file.write(
                        "{} {} {:.2f} {:.2f} {:.2f} {:.2f} {:.2e} {:.2e} {:.1f} {:.2f} {:.2e}\n".format(
                            cme["model"],
                            cme["datetime"],
                            float(cme["latitude"]),
                            float(cme["longitude"]),
                            float(cme["radius"]),
                            float(cme["speed"]),
                            Decimal(cme["density"]),
                            Decimal(cme["temperature"]),
                            float(cme["chirality"]),
                            float(cme["tilt"]),
                            Decimal(cme["flux"]),
                        )
                    )
                elif cme["model"] == "FRi3D":
                    config_file.write(
                        "{} {} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.1f} {:.1f} {:.2e} {:.2e} {:.2e}\n".format(
                            cme["model"],
                            cme["datetime"],
                            float(cme["latitude"]),
                            float(cme["longitude"]),
                            float(cme["speed"]),
                            float(cme["toroidalHeight"]),
                            float(cme["halfWidth"]),
                            float(cme["halfHeight"]),
                            float(cme["tilt"]),
                            float(cme["flattening"]),
                            float(cme["pancaking"]),
                            float(cme["twist"]),
                            float(cme["polarity"]),
                            float(cme["chirality"]),
                            Decimal(cme["flux"]),
                            Decimal(cme["density"]),
                            Decimal(cme["temperature"]),
                        )
                    )
        # cluster config
        with open(self._cluster_config_path, "w") as config_file:
            config = dict(self._job["run"]["config"]["heliospheric_model"]["cluster"])
            config_file.write("#!/bin/bash -l\n")
            config_file.write("#SBATCH --account={}\n".format(self._default_account))
            config_file.write("#SBATCH --cluster=wice\n")
            config_file.write("#SBATCH --nodes={:d}\n".format(int(config["nodes"])))
            # config_file.write("#SBATCH --ntasks-per-node={:d}\n".format(int(config["ppn"])))
            config_file.write("#SBATCH --ntasks-per-node=72\n")
            config_file.write(
                "#SBATCH --time={:02d}:{:02d}:00\n".format(int(config["walltime"][:2]), int(config["walltime"][2:]))
            )
            config_file.write("#SBATCH --output={}\n".format(os.path.join(self._path, "log.txt")))
            config_file.write("#SBATCH --job-name=euhforiaonline-{}\n".format(self._job["id"]))

            for line in self._get_stack():
                config_file.write("{}\n".format(line))

            config_file.write(
                "mpirun -np {:d} {} --config-file {} >> {} 2>&1\n".format(
                    # int(config["nodes"]) * int(config["ppn"]),
                    int(config["nodes"]) * 72,
                    os.path.join(self._euhforia_path, "run", "heliosphere.py"),
                    self._heliosphere_config_path,
                    os.path.join(self._path, "log.txt"),
                )
            )
            config_file.write(
                "find {} -name '*.npz' -print0 | xargs -0 -I {{}} basename {{}} | sed -E 's/.*_([0-9]{{4}}-[0-9]{{2}}-[0-9]{{2}}T[0-9]{{2}}-[0-9]{{2}}-[0-9]{{2}}).*/\\1/' | stdbuf -oL awk '!seen[$0]++' | while read -r datetime; do python -W ignore {} \"{}\" --output_dir {} --meridional_plane {} --lowres >> {} 2>&1 & done\n".format(
                    self._heliosphere_path,
                    os.path.join(self._euhforia_path, "run", "create_plots.py"),
                    os.path.join(self._heliosphere_path, "*${datetime}*.npz"),
                    self._heliosphere_path + "/",
                    self._job["run"]["config"]["heliospheric_model"]["plotting"]["meridional_plane"],
                    os.path.join(self._path, "log.txt"),
                )
            )
            config_file.write("wait\n")
            config_file.write(
                "{} {} --output_dir {} >> {} 2>&1\n".format(
                    os.path.join(self._euhforia_path, "run", "create_insitu_plots.py"),
                    os.path.join(self._heliosphere_path, "*.dsv"),
                    self._heliosphere_path + "/",
                    os.path.join(self._path, "log.txt"),
                )
            )

    def start_coronal_model(self):
        output = check_output(["sbatch", self._cluster_corona_config_path])
        job_id = re.search(r"(\d+)", output.decode()).group(1)
        return job_id

    def start_heliospheric_model(self):
        output = check_output(["sbatch", self._cluster_config_path])
        job_id = re.search(r"(\d+)", output.decode()).group(1)
        return job_id

    def run_coronal_model(self):
        self.configure_coronal_model()
        job_id = self.start_coronal_model()
        self.update(status=self.STATUS_QUEUED, job_id=job_id)

    def run_heliospheric_model(self):
        self.configure_heliospheric_model()
        job_id = self.start_heliospheric_model()
        self.update(status=self.STATUS_QUEUED, job_id=job_id)

    def get_status(self):
        status = self.STATUS_FAILED
        sacct_output = check_output(
            ["sacct", "-P", "-o", "JobID,State", "-M", "wice", "-j", self._job["jobId"]]
        ).decode()
        lines = sacct_output.strip().split("\n")
        sacct_status = lines[1].split("|")[1]
        if "RUNNING" in sacct_status or "RESIZING" in sacct_status:
            status = self.STATUS_RUNNING
        elif "COMPLETED" in sacct_status:
            status = self.STATUS_DONE
        elif "PENDING" in sacct_status or "REQUEUED" in sacct_status or "SUSPENDED" in sacct_status:
            status = self.STATUS_QUEUED
        else:
            status = self.STATUS_FAILED
        return status

    def upload_data(self, filepath, code, secret_key=envconfig("EUHFORIAONLINE_SECRET_KEY")):
        try:
            query = """
                mutation($dataData: DataInput!) {
                    uploadData(dataData: $dataData) {
                        id
                    }
                }
            """
            variables = {"dataData": {"runId": self._job["run"]["id"], "code": code, "file": None}}
            json_response = requests.post(
                self._graphql_endpoint,
                data={
                    "operations": json.dumps({"query": query, "variables": variables}),
                    "map": json.dumps({"0": ["variables.dataData.file"]}),
                },
                files={"0": open(filepath, "rb")},
                headers={"Authorization": secret_key},
            ).json()
            return json_response["data"]["uploadData"]
        except Exception as e:
            print("Failed to upload data: ", filepath + "\n" + str(e))

    def upload_log(self):
        log_path = os.path.join(self._path, "log.txt")
        if Path(log_path).is_file():
            self.upload_data(log_path, "LOGF")

    def upload_coronal_output(self):
        basepath = os.path.splitext(self._solar_wind_boundary_path)[0]
        self.upload_data(self._solar_wind_boundary_path, "SWBF")
        self.upload_data(basepath + "-magnetogram.png", "MAGP")
        self.upload_data(basepath + "-open_and_closed_field_regions.png", "OCFP")
        self.upload_data(basepath + "-expansion_factor.png", "FTEP")
        self.upload_data(basepath + "-radial_velocity.png", "RSBP")
        self.upload_data(basepath + "-number_density.png", "NDBP")
        self.upload_data(basepath + "-radial_field.png", "RFBP")
        self.upload_data(basepath + "-temperature.png", "TMBP")

    def upload_heliospheric_output(self):
        for plot_path in glob(os.path.join(self._heliosphere_path, "vr_*.png")):
            self.upload_data(plot_path, "RSCP")
        for plot_path in glob(os.path.join(self._heliosphere_path, "nscaled_*.png")):
            self.upload_data(plot_path, "NDCP")
        vantage_codes = {
            "SC{}".format(i + 1): {"plot": "SC{}P".format(i + 1), "data": "SC{}F".format(i + 1)} for i in range(100)
        }
        vantage_codes["Earth"] = {"plot": "EAIP", "data": "EAIF"}
        vantage_codes["Mercury"] = {"plot": "MEIP", "data": "MEIF"}
        vantage_codes["Mars"] = {"plot": "MAIP", "data": "MAIF"}
        vantage_codes["Venus"] = {"plot": "VEIP", "data": "VEIF"}
        vantage_codes["STA"] = {"plot": "SAIP", "data": "SAIF"}
        vantage_codes["STB"] = {"plot": "SBIP", "data": "SBIF"}
        for vantage_point in vantage_codes:
            base_path = os.path.join(
                self._heliosphere_path, "_".join([self._job["run"]["id"], self._run_name, vantage_point])
            )
            plot_path = base_path + ".png"
            data_path = base_path + ".dsv"
            if Path(plot_path).is_file() and Path(data_path).is_file():
                self.upload_data(plot_path, vantage_codes[vantage_point]["plot"])
                self.upload_data(data_path, vantage_codes[vantage_point]["data"])

    def terminate(self):
        if self._job["jobId"] is not None:
            Popen(["scancel", "--cluster=wice", self._job["jobId"]])
        self.update(status=self.STATUS_CANCELLED)

    def clear(self):
        rmtree(Path(self._path))

    @staticmethod
    def all(
        status=None,
        graphql_endpoint=envconfig(
            "EUHFORIAONLINE_GRAPHQL_ENDPOINT", default="https://api.euhforiaonline.com/graphql/"
        ),
        secret_key=envconfig("EUHFORIAONLINE_SECRET_KEY"),
    ):
        query = """
            query($status: String) {
                allJobs(status: $status) {
                    id
                    model
                    status
                    jobId
                    run {
                        id
                        config
                    }
                    user {
                        id
                        email
                    }
                    updatedAt
                }
            }
        """
        variables = {}
        if status is not None:
            variables["status"] = status
        json_response = requests.post(
            graphql_endpoint, json={"query": query, "variables": variables}, headers={"Authorization": secret_key}
        ).json()
        jobs = json_response["data"]["allJobs"]
        job_objects = []
        for index, job in enumerate(jobs):
            if job["run"] is not None:
                jobs[index]["run"]["config"] = json.loads(job["run"]["config"])
                job_objects.append(Job(jobs[index]))
        return job_objects


def get_my_username():
    return check_output("echo $USER", shell=True).decode().rstrip()


def get_my_balance():
    balance_raw = check_output("sam-balance", shell=True).decode()
    lines = balance_raw.strip().split("\n")
    lines = lines[2:]
    balance = {}
    for line in lines:
        if line.strip():
            parts = line.split()
            try:
                balance.update({parts[1]: int(parts[4])})
            except ValueError:
                balance.update({parts[1]: -1})
    return balance


def update_my_balance(
    vsc_balance,
    graphql_endpoint=envconfig("EUHFORIAONLINE_GRAPHQL_ENDPOINT", default="https://api.euhforiaonline.com/graphql/"),
    secret_key=envconfig("EUHFORIAONLINE_SECRET_KEY"),
):
    vsc_id = get_my_username()
    query = """
        mutation($myBalanceData: MyBalanceInput!) {
            updateMyBalance(myBalanceData: $myBalanceData) {
                id
                vscBalance
            }
        }
    """
    variables = {"myBalanceData": {"vscId": vsc_id, "vscBalance": json.dumps(vsc_balance)}}
    json_response = requests.post(
        graphql_endpoint, json={"query": query, "variables": variables}, headers={"Authorization": secret_key}
    ).json()
    return json_response


def process_my_balance():
    vsc_balance = get_my_balance()
    update_my_balance(vsc_balance)


def process_terminated_jobs(clear=envconfig("EUHFORIAONLINE_CLEAR", default=True, cast=bool)):
    jobs = Job.all(status=Job.STATUS_TERMINATED)
    for job in jobs:
        job.terminate()
        if clear:
            job.clear()
        job.upload_log()


def process_submitted_jobs():
    jobs = Job.all(status=Job.STATUS_SUBMITTED)
    for job in jobs:
        if job.is_coronal():
            job.run_coronal_model()
        elif job.is_heliospheric():
            job.run_heliospheric_model()


def process_queued_jobs(clear=envconfig("EUHFORIAONLINE_CLEAR", default=True, cast=bool)):
    jobs = Job.all(status=Job.STATUS_QUEUED)
    for job in jobs:
        if job.is_coronal():
            status = job.get_status()
            if status == Job.STATUS_RUNNING:
                job.update(status=Job.STATUS_RUNNING)
            elif status == Job.STATUS_DONE:
                job.upload_coronal_output()
                job.update(status=Job.STATUS_DONE)
                if job.to_run_heliospheric():
                    next_job = job.create_heliospheric_job()
                    next_job.run_heliospheric_model()
                else:
                    if clear:
                        job.clear()
            elif status == Job.STATUS_FAILED:
                job.update(status=Job.STATUS_FAILED)
                if clear:
                    job.clear()
        if job.is_heliospheric():
            status = job.get_status()
            if status == Job.STATUS_RUNNING:
                job.update(status=Job.STATUS_RUNNING)
            elif status == Job.STATUS_DONE:
                job.upload_heliospheric_output()
                job.update(status=Job.STATUS_DONE)
                if clear:
                    job.clear()
            elif status == Job.STATUS_FAILED:
                job.update(status=Job.STATUS_FAILED)
                if clear:
                    job.clear()
            job.upload_log()


def process_running_jobs(clear=envconfig("EUHFORIAONLINE_CLEAR", default=True, cast=bool)):
    jobs = Job.all(status=Job.STATUS_RUNNING)
    for job in jobs:
        if job.is_coronal():
            status = job.get_status()
            if status == Job.STATUS_DONE:
                job.upload_coronal_output()
                job.update(status=Job.STATUS_DONE)
                if job.to_run_heliospheric():
                    next_job = job.create_heliospheric_job()
                    next_job.run_heliospheric_model()
                else:
                    if clear:
                        job.clear()
            elif status == Job.STATUS_FAILED:
                job.update(status=Job.STATUS_FAILED)
                if clear:
                    job.clear()
        elif job.is_heliospheric():
            status = job.get_status()
            if status == Job.STATUS_DONE:
                job.upload_heliospheric_output()
                job.update(status=Job.STATUS_DONE)
                if clear:
                    job.clear()
            elif status == Job.STATUS_FAILED:
                job.update(status=Job.STATUS_FAILED)
                if clear:
                    job.clear()
        job.upload_log()
