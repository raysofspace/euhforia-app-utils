import os
import re

from setuptools import find_packages, setup
from setuptools.command.develop import develop
from setuptools.command.install import install

version_py = open(os.path.join(os.path.dirname(__file__), "version.py")).read().strip().split("=")[-1].replace('"', "")

EUHFORIAONLINE_PROCESS_TERMINATED_JOBS = "EUHFORIAONLINE_PROCESS_TERMINATED_JOBS"
EUHFORIAONLINE_PROCESS_SUBMITTED_JOBS = "EUHFORIAONLINE_PROCESS_SUBMITTED_JOBS"
EUHFORIAONLINE_PROCESS_MY_BALANCE = "EUHFORIAONLINE_PROCESS_MY_BALANCE"
EUHFORIAONLINE_PROCESS_QUEUED_JOBS = "EUHFORIAONLINE_PROCESS_QUEUED_JOBS"
EUHFORIAONLINE_PROCESS_RUNNING_JOBS = "EUHFORIAONLINE_PROCESS_RUNNING_JOBS"


def with_post_install(command):
    original_run = command.run

    def modified_run(self):
        original_run(self)
        from crontab import CronTab

        cron = CronTab(user=True)
        cron.remove_all(comment=re.compile("EUHFORIAONLINE_PROCESS_*"))
        job = cron.new(
            os.path.join("$HOME", ".local", "bin", "euhforiaonline_process_terminated_jobs") + " &>/dev/null",
            comment=EUHFORIAONLINE_PROCESS_TERMINATED_JOBS,
        )
        job.minute.every(5)
        job = cron.new(
            os.path.join("$HOME", ".local", "bin", "euhforiaonline_process_submitted_jobs") + " &>/dev/null",
            comment=EUHFORIAONLINE_PROCESS_SUBMITTED_JOBS,
        )
        job.minute.every(15)
        job = cron.new(
            os.path.join("$HOME", ".local", "bin", "euhforiaonline_process_my_balance") + " &>/dev/null",
            comment=EUHFORIAONLINE_PROCESS_MY_BALANCE,
        )
        job.minute.every(30)
        job = cron.new(
            os.path.join("$HOME", ".local", "bin", "euhforiaonline_process_queued_jobs") + " &>/dev/null",
            comment=EUHFORIAONLINE_PROCESS_QUEUED_JOBS,
        )
        job.minute.every(30)
        job = cron.new(
            os.path.join("$HOME", ".local", "bin", "euhforiaonline_process_running_jobs") + " &>/dev/null",
            comment=EUHFORIAONLINE_PROCESS_RUNNING_JOBS,
        )
        job.minute.every(30)
        cron.write_to_user(user=True)

    command.run = modified_run
    return command


@with_post_install
class PostDevelopCommand(develop):
    pass


@with_post_install
class PostInstallCommand(install):
    pass


setup(
    name="euhforiaonline",
    version="{ver}".format(ver=version_py),
    description="Cluster-side portion of EUHFORIA Online web service",
    author="Rays of Space Oy",
    author_email="info@raysofspace.com",
    license="MIT",
    packages=find_packages("src", exclude=["test*"]),
    package_dir={"": "src"},
    scripts=[
        "src/bin/euhforiaonline_process_terminated_jobs",
        "src/bin/euhforiaonline_process_submitted_jobs",
        "src/bin/euhforiaonline_process_my_balance",
        "src/bin/euhforiaonline_process_queued_jobs",
        "src/bin/euhforiaonline_process_running_jobs",
    ],
    install_requires=["requests", "python-decouple", "python-crontab"],
    setup_requires=["python-crontab"],
    cmdclass={"develop": PostDevelopCommand, "install": PostInstallCommand},
)
